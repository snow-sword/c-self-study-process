#include <iostream>
using namespace std;
int stonetolb(int);    //定义一个会返回整数值的函数
int main()
{
    int stone;
    cout << "Enter the weight in stone: ";
    cin >> stone;
    int pounds = stonetolb(stone);   //将stonetolb返回的整数值赋值给pounds
    cout << stone << " stone = ";
    cout << pounds << " pounds." << endl;
    return 0;
}

int stonetolb(int sts)   //使用该函数时必须提供一个int类型参数
{
    return 14 * sts;    //返回14乘以参数sts
}

