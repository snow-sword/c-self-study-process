#include <iostream>
void Snow(int);     //定义一个函数名
using namespace std;  //放在开头就不怕后面忘了写
int main()    //int表示main函数将会返回int（整数值）
{
   
    Snow(3);      //将3赋值给Snow中的“n”，并运行一次Snow函数
    cout << "Pick an integer:";
    int count;
    cin >> count;
    Snow(count);       //将count的值赋值给“n”并运行一次Snow函数
    cout << "Done!" << endl;
    return 0;     //main函数返回“0”这个整数值
}

void Snow(int n)      //void表示Snow函数没有返回值，n表示使用Snow时需要提供一个int参数
{
    cout << "Simon says touch your toes " << n << " times." << endl;
} 