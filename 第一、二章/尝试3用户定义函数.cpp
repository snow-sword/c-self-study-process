#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
void Snowsword(int);//无返回值的函数
int stonetolb(int);//有返回值的函数
int main()//main函数放在最前面，用来运行
{
    //无返回值的函数
        srand((unsigned)time(NULL));
        Snowsword(3);//运行一次参数为3的Snowsword函数
        int count = rand();
        for(int i=0;i<5;i++)//运行五次参数逐加的Snowsword函数
        {
            Snowsword(count);
            count = rand();
        }
    //有返回值的函数

        int stone;
        cout << "Enter the weight in stone:";
        cin >> stone;
        int pounds = stonetolb(stone);//将stonetolb的返回值赋予 pounds整数
        cout << stone << "stone=";
        cout << pounds << "pounds." <<endl;
    
        
    return 0; 
}

void Snowsword(int a)//main里面赋予Snowsword的参数将被赋予在 a ，以下的 sts 同理
{
    cout << "Can I say the new number?"<<endl;
    cout << a << endl;
}

int stonetolb(int sts)//有返回值的函数
{
    return 14 * sts;//返回sts参数的14倍数值
}
/*
    用户定义的函数格式离不开:
        type functionname(argumentlist)
        {
            statements
        }
*/